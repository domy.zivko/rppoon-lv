﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV5_Z6
{
	class GroupNote : Note
	{
		private readonly List<string> group = new List<string>();

		public GroupNote(string message, ITheme theme) : base(message, theme) { }

		public void AddPerson(string name)
		{
			group.Add(name);
		}

		public void RemovePerson(string name)
		{
			group.Remove(name);
		}

		public override void Show()
		{
			ChangeColor();
			ShowGroupMembers();

			string framedMessage = GetFramedMessage();
			Console.WriteLine(framedMessage);
			Console.ResetColor();
		}

		private void ShowGroupMembers()
		{
			Console.Write("Note for: ");
			for (int i = 0; i < group.Count; i++)
			{
				Console.Write(group[i]);
				if (i < group.Count - 1) Console.Write(", ");
			}
			Console.WriteLine();
		}
	}
}
