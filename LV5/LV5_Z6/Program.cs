﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV5_Z6
{
	class Program
	{
		static void Main(string[] args)
		{
			ITheme lightTheme = new LightTheme();
			ITheme sunriseTheme = new SunriseTheme();

			ReminderNote firstReminder = new ReminderNote("Set repository to public", lightTheme);
			ReminderNote secondReminder = new ReminderNote("Get a haircut", sunriseTheme);

			GroupNote noteToAdmins = new GroupNote("Prepare maintanance procedure", lightTheme);
			noteToAdmins.AddPerson("Richard");
			noteToAdmins.AddPerson("Victoria");

			GroupNote noteToUsers = new GroupNote("Server will be shut down for maintanance overnight", sunriseTheme);
			noteToUsers.AddPerson("Phil");
			noteToUsers.AddPerson("Susan");
			noteToUsers.AddPerson("Karen");
			noteToUsers.AddPerson("Tom");

			firstReminder.Show();
			secondReminder.Show();
			Console.WriteLine();
			noteToAdmins.Show();
			noteToUsers.Show();
		}
	}
}
