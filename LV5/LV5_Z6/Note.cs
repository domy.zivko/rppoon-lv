﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV5_Z6
{
	abstract class Note
	{
		private string message;
		private ITheme theme;

		public Note(string message, ITheme theme)
		{
			this.message = message;
			this.theme = theme;
		}

		public string Message { get { return this.message; } }
		public ITheme Theme { set { this.theme = value; } }

		protected void ChangeColor()
		{
			theme.SetBackgroundColor();
			theme.SetFontColor();
		}

		protected string GetFramedMessage()
		{
			int width = message.Length;
			return theme.GetHeader(width) + "\n" + message + "\n" + theme.GetFooter(width);
		}

		public abstract void Show();
	}
}
