﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV5_Z7
{
	class ReminderNote : Note
	{
		public ReminderNote(string message, ITheme theme) : base(message, theme) { }

		public override void Show()
		{
			ChangeColor();
			Console.WriteLine("REMINDER: ");
			string framedMessage = GetFramedMessage();

			Console.WriteLine(framedMessage);
			Console.ResetColor();
		}
	}
}
