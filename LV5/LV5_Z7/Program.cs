﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV5_Z7
{
	class Program
	{
		readonly ITheme lightTheme = new LightTheme();
		readonly ITheme sunriseTheme = new SunriseTheme();

		Program()
		{
			Notebook firstNotebook = BuildNotebook();
			firstNotebook.Display();
			Console.WriteLine("====================\n");

			firstNotebook.ChangeTheme(lightTheme);
			firstNotebook.Display();
			Console.WriteLine("====================\n");

			Notebook secondNotebook = BuildNotebook(sunriseTheme);
			secondNotebook.Display();
		}

		private Notebook BuildNotebook()
		{
			return BuildNotebook(null);
		}

		private Notebook BuildNotebook(ITheme defaultTheme)
		{
			Notebook notebook = new Notebook(defaultTheme);

			ReminderNote firstReminder = new ReminderNote("Set repository to public", lightTheme);
			ReminderNote secondReminder = new ReminderNote("Get a haircut", sunriseTheme);

			GroupNote noteToAdmins = new GroupNote("Prepare maintanance procedure", lightTheme);
			noteToAdmins.AddPerson("Richard");
			noteToAdmins.AddPerson("Victoria");

			GroupNote noteToUsers = new GroupNote("Server will be shut down for maintanance overnight", sunriseTheme);
			noteToUsers.AddPerson("Phil");
			noteToUsers.AddPerson("Susan");
			noteToUsers.AddPerson("Karen");
			noteToUsers.AddPerson("Tom");

			notebook.AddNote(firstReminder);
			notebook.AddNote(secondReminder);
			notebook.AddNote(noteToAdmins);
			notebook.AddNote(noteToUsers);

			return notebook;
		}


		static void Main(string[] args)
		{
			new Program();
		}
	}
}
