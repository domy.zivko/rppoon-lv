﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV5_Z7
{
	class Notebook
	{
		private List<Note> notes;
		private ITheme defaultTheme;

		public Notebook(ITheme defaultTheme) : this()
		{
			this.defaultTheme = defaultTheme;
		}

		public Notebook()
		{
			this.notes = new List<Note>();
		}

		public void AddNote(Note note)
		{
			if (defaultTheme != null)
				note.Theme = defaultTheme;

			notes.Add(note);
		}

		public void ChangeTheme(ITheme theme)
		{
			foreach (Note note in notes)
			{
				note.Theme = theme;
			}
		}

		public void Display()
		{
			foreach (Note note in notes)
			{
				note.Show();
				Console.WriteLine("\n");
			}
		}
	}
}
