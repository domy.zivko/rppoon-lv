﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV5_Z3
{
	class Program
	{
		static void Main(string[] args)
		{
			DataConsolePrinter printer = new DataConsolePrinter();

			VirtualProxyDataset virtualProxyDataset = new VirtualProxyDataset("openData.csv");
			printer.Print(virtualProxyDataset);
			Console.WriteLine();

			User firstUser = User.GenerateUser("User One");
			User secondUser = User.GenerateUser("User Two");

			ProtectionProxyDataset protectionProxyDataset = new ProtectionProxyDataset(firstUser);
			printer.Print(protectionProxyDataset);
			Console.WriteLine();
			protectionProxyDataset.user = secondUser;
			printer.Print(protectionProxyDataset);
		}
	}
}
