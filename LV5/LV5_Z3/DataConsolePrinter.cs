﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV5_Z3
{
	class DataConsolePrinter
	{
		public void Print(IDataset dataset)
		{
			ReadOnlyCollection<List<string>> data = dataset.GetData();
			if (data == null) Console.WriteLine("No data available");
			else
			{
				foreach (List<string> row in data)
				{
					foreach(string item in row)
					{
						Console.Write(item + " ");
					}
					Console.WriteLine();
				}

			}
		}
	}
}
