﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV5_Z2
{
	class Program
	{
		static void Main(string[] args)
		{
			Box orderedProducts = new Box("Order #001");

			Box pots = new Box("Cooking pots");
			pots.Add(new Product("Large cooking pot", 1.1, 49.99));
			pots.Add(new Product("Small cooking pot", 0.5, 29.99));

			Box toiletries = new Box("Toiletries");
			toiletries.Add(new Product("Toilet paper", 0.5, 18.99));
			toiletries.Add(new Product("Green toothbrush", 0.015, 8.99));
			toiletries.Add(new Product("Hand soap", 0.3, 16.49));

			Product handSanitizer = new Product("Hand sanitizer", 0.2, 34.99);

			orderedProducts.Add(pots);
			orderedProducts.Add(toiletries);
			orderedProducts.Add(handSanitizer);

			ShippingService shippingService = new ShippingService(10);
			PrintOrder(orderedProducts, shippingService);
		}

		private static void PrintOrder(IShipable item, ShippingService shippingService)
		{
			PrintItem(item);
			double shippingFees = shippingService.CalculateShippingFees(item);
			Console.WriteLine("Shipping fees: " + shippingFees);
			Console.WriteLine("----------");
			Console.WriteLine("Total price: " + (item.Price + shippingFees));
		}

		private static void PrintItem(IShipable item)
		{
			Console.WriteLine(item.Description());
			Console.WriteLine("----------");
			Console.WriteLine("Weight: " + item.Weight);
			Console.WriteLine("Price: " + item.Price);
		}
	}
}
