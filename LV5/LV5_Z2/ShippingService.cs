﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV5_Z2
{
	class ShippingService
	{
		private double pricePerKilogram;

		public ShippingService(double pricePerKilogram)
		{
			this.pricePerKilogram = pricePerKilogram;
		}

		public double CalculateShippingFees(IShipable item)
		{
			return item.Weight * pricePerKilogram;
		}
	}
}
