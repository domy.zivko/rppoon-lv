﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV5_Z2
{
	interface IShipable
	{
		double Price { get; }
		double Weight { get; }
		string Description(int depth = 0);
	}
}
