﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV5_Z4
{
	class Program
	{
		static void Main(string[] args)
		{

			Dataset dataset = new Dataset("sensitiveData.csv");
			LoggingProxyDataset loggingDataset = new LoggingProxyDataset(dataset);

			DataConsolePrinter printer = new DataConsolePrinter();
			printer.Print(loggingDataset);
		}
	}
}
