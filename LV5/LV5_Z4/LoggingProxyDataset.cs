﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV5_Z4
{
	class LoggingProxyDataset : IDataset
	{
		private Dataset dataset;

		public LoggingProxyDataset(Dataset dataset)
		{
			this.dataset = dataset;
		}

		public ReadOnlyCollection<List<string>> GetData()
		{
			ConsoleLogger.GetInstance().Log(DateTime.Now + " - dataset accessed");
			return dataset.GetData();
		}
	}
}
