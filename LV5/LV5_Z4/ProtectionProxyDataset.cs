﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV5_Z4
{
	class ProtectionProxyDataset : IDataset
	{
		private Dataset dataset;
		private List<int> allowerdIDs;
		public User user { private get; set; }

		public ProtectionProxyDataset(User user)
		{
			this.allowerdIDs = new List<int>(new int[] { 1, 3, 5 });
			this.user = user;
		}

		private bool AuthenticateUser()
		{
			return allowerdIDs.Contains(user.ID);
		}

		public ReadOnlyCollection<List<string>> GetData()
		{
			if (AuthenticateUser())
			{
				if(dataset == null)
				{
					dataset = new Dataset("sensitiveData.csv");
				}
				return dataset.GetData();
			}

			return null;
		}
	}
}
