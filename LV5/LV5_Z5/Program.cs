﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV5_Z5
{
	class Program
	{
		static void Main(string[] args)
		{
			ITheme lightTheme = new LightTheme();
			ITheme sunriseTheme = new SunriseTheme();

			ReminderNote firstReminder = new ReminderNote("Set repository to public", lightTheme);
			ReminderNote secondReminder = new ReminderNote("Get a haircut", sunriseTheme);

			firstReminder.Show();
			secondReminder.Show();
		}
	}
}
