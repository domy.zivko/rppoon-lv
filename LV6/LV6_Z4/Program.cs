﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV6_Z4
{
	class Program
	{
		static void Main(string[] args)
		{
			BankAccount account = new BankAccount("Dominik Živko", "pending", Decimal.MaxValue);
			Console.WriteLine(account);

			Memento state = account.StoreState();
			account.ChangeOwnerAddress("unknown");
			account.UpdateBalance(-111111);
			Console.WriteLine(account);

			account.RestoreState(state);
			Console.WriteLine(account);
		}
	}
}
