﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV6_Z4
{
	class BankAccount
	{
		private string ownerName;
		private string ownerAddress;
		private decimal balance;

		public BankAccount(string ownerName, string ownerAddress, decimal balance)
		{
			this.ownerName = ownerName;
			this.ownerAddress = ownerAddress;
			this.balance = balance;
		}

		public void ChangeOwnerAddress(string address)
		{
			this.ownerAddress = address;
		}

		public void UpdateBalance(decimal amount)
		{
			this.balance += amount;
		}

		public string OwnerName { get { return ownerName; } }
		public string OwnerAddress { get { return ownerAddress; } }
		public decimal Balance { get { return balance; } }

		public override string ToString()
		{
			return $"{OwnerName} - {OwnerAddress}:\n{Balance}";
		}

		public Memento StoreState()
		{
			return new Memento(ownerName, ownerAddress, balance);
		}

		public void RestoreState(Memento previous)
		{
			ownerName = previous.OwnerName;
			ownerAddress = previous.OwnerAddress;
			balance = previous.Balance;
		}
	}
}
