﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV6_Z5
{
	abstract class AbstractLogger
	{
		protected MessageType messageTypeHandling;
		protected AbstractLogger nextLogger;

		public AbstractLogger(MessageType messageType)
		{
			this.messageTypeHandling = messageType;
		}

		public void SetNextLogger(AbstractLogger logger)
		{
			this.nextLogger = logger;
		}

		public void Log(string message, MessageType type)
		{
			if ((type & this.messageTypeHandling) != 0)
			{
				WriteMessage(message, type);
			}

			if (nextLogger != null)
			{
				nextLogger.Log(message, type);
			}
		}

		protected abstract void WriteMessage(string message, MessageType type);
	}
}
