﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV6_Z5
{
	class Program
	{
		static void Main(string[] args)
		{
			AbstractLogger logger = new ConsoleLogger(MessageType.ALL);
			FileLogger fileLogger = new FileLogger(MessageType.ERROR | MessageType.WARNING, "logFile.txt");
			logger.SetNextLogger(fileLogger);

			logger.Log("Info test", MessageType.INFO);
			logger.Log("Warning test", MessageType.WARNING);
			logger.Log("Error test", MessageType.ERROR);
		}
	}
}
