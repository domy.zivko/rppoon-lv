﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV6_Z3
{
	class CareTaker
	{
		private readonly Stack<Memento> mementos = new Stack<Memento>();

		public Memento PopMemento()
		{
			return mementos.Pop();
		}

		public void PushMemento(Memento memento)
		{
			mementos.Push(memento);
		}
	}
}
