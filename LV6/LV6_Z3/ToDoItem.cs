﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV6_Z3
{
	class ToDoItem
	{
		private string title;
		private string text;
		private DateTime timeDue;
		public DateTime CreationTime { get; private set; }

		public ToDoItem(string title, string text, DateTime timeDue)
		{
			this.title = title;
			this.text = text;
			this.timeDue = timeDue;
			this.CreationTime = DateTime.Now;
		}

		public void Rename(string title)
		{
			this.title = title;
		}

		public void ChangeTask(string text)
		{
			this.text = text;
		}

		public void ChangeTimeDue(DateTime timeDue)
		{
			this.timeDue = timeDue;
		}

		public override string ToString()
		{
			return title + " - due by: " + timeDue + "\n" + text;
		}

		public Memento StoreState()
		{
			return new Memento(title, text, timeDue, CreationTime);
		}

		public void RestoreState(Memento previous)
		{
			title = previous.Title;
			text = previous.Text;
			timeDue = previous.TimeDue;
			CreationTime = previous.CreationTime;
		}
	}
}
