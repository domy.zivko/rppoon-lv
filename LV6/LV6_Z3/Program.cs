﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV6_Z3
{
	class Program
	{
		static void Main(string[] args)
		{
			CareTaker careTaker = new CareTaker();
			ToDoItem item = new ToDoItem("To Buy", "Milk, Eggs", DateTime.Now.AddDays(5));
			Console.WriteLine(item + "\n");

			careTaker.PushMemento(item.StoreState());
			item.Rename("Shopping");
			Console.WriteLine(item + "\n");

			careTaker.PushMemento(item.StoreState());
			item.ChangeTask("Milk, Eggs, Bread");
			item.ChangeTimeDue(DateTime.Now.AddDays(2));
			Console.WriteLine(item + "\n");

			item.RestoreState(careTaker.PopMemento());
			Console.WriteLine("Undo:\n" + item + "\n");
			item.RestoreState(careTaker.PopMemento());
			Console.WriteLine("Undo:\n" + item + "\n");
		}
	}
}
