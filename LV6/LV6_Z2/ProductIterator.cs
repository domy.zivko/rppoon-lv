﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV6_Z2
{
	class ProductIterator : IAbstractProductIterator
	{
		private Box box;
		private int currentPosition;

		public ProductIterator(Box box)
		{
			this.box = box;
			this.currentPosition = 0;
		}

		public bool IsDone {
			get { return currentPosition >= box.Count; }
		}

		public Product Current {
			get { return box[currentPosition]; }
		}

		public Product First()
		{
			return box[0];
		}

		public Product Next()
		{
			currentPosition++;
			return IsDone ? null : box[currentPosition];
		}
	}
}
