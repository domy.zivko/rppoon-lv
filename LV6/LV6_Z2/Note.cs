﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV6_Z2
{
	class Note
	{
		public string Title { get; private set; }
		public string Text { get; private set; }

		public Note(string title, string text)
		{
			this.Title = title;
			this.Text = text;
		}

		private int GetFrameWidth()
		{
			return Title.Length < Text.Length ? Text.Length : Title.Length;
		}

		private string GetRule(char c)
		{
			return new string(c, GetFrameWidth());
		}

		public void Show()
		{
			Console.WriteLine(GetRule('='));
			Console.WriteLine(Title);
			Console.WriteLine(GetRule('-'));
			Console.WriteLine(Text);
			Console.WriteLine(GetRule('='));
		}
	}
}
