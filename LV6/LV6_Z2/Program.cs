﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV6_Z2
{
	class Program
	{
		static void Main(string[] args)
		{
			// Uzas. Iterator koji radi samo s Note objektima, i posebni ProductIterator za Product.
			// Klase iz prvog zadatka sam stavio i ovdje samo zato sto se u dodatku to podrazumijevalo. 
			// Inace ne bi bilo potrebe za novim suceljima.

			Box box = new Box();
			box.AddProduct(new Product("Item 1", 1.0));
			box.AddProduct(new Product("Item 2", 2.0));
			box.AddProduct(new Product("Can't be bothered", 1.23));

			IAbstractProductIterator it = box.GetIterator();
			for (Product product = it.First(); !it.IsDone; product = it.Next())
			{
				Console.WriteLine(product);
			}
		}
	}
}
