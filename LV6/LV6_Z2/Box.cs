﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV6_Z2
{
	class Box : IAbstractProductCollection
	{
		private List<Product> products;

		public Box()
		{
			this.products = new List<Product>();
		}

		public Box(List<Product> products)
		{
			this.products = new List<Product>(products.ToArray());
		}

		public void AddProduct(Product product)
		{
			products.Add(product);
		}

		public void RemoveProduct(Product product)
		{
			products.Remove(product);
		}

		public int Count {
			get { return products.Count; }
		}

		public Product this[int index] {
			get { return products[index]; }
		}

		public IAbstractProductIterator GetIterator()
		{
			return new ProductIterator(this);
		}
	}
}
