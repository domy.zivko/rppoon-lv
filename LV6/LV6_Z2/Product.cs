﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV6_Z2
{
	class Product
	{
		public string Description { get; private set; }
		public double Price { get; private set; }

		public Product(string desscription, double price)
		{
			this.Description = desscription;
			this.Price = price;
		}

		public override string ToString()
		{
			return Description + ":\n" + Price;
		}
	}
}
