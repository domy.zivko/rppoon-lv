﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV6_Z2
{
	class Iterator : IAbstractIterator
	{
		private Notebook notebook;
		private int currentPosition;

		public Iterator(Notebook notebook)
		{
			this.notebook = notebook;
			this.currentPosition = 0;
		}

		public bool IsDone {
			get { return currentPosition >= notebook.Count; }
		}

		public Note Current {
			get { return notebook[currentPosition]; }
		}

		public Note First()
		{
			return notebook[0];
		}

		public Note Next()
		{
			currentPosition++;
			return IsDone ? null : notebook[currentPosition];
		}
	}
}
