﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV6_Z6
{
	class Program
	{
		static void Main(string[] args)
		{
			StringChecker checker = new StringLengthChecker(6);
			StringDigitChecker digitChecker = new StringDigitChecker();
			StringUpperCaseChecker upperCaseChecker = new StringUpperCaseChecker();
			StringLowerCaseChecker lowerCaseChecker = new StringLowerCaseChecker();

			checker.SetNext(digitChecker);
			digitChecker.SetNext(upperCaseChecker);
			upperCaseChecker.SetNext(lowerCaseChecker);

			string str1 = "This 1 will pass";
			string str2 = "This will not pass";
			Console.Write(str1 + ": ");
			Console.WriteLine(checker.Check(str1) ? "passed" : "failed");
			Console.Write(str2 + ": ");
			Console.WriteLine(checker.Check(str2) ? "passed" : "failed");
		}
	}
}
