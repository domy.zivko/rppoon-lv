﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV6_Z1
{
	class Program
	{
		static void Main(string[] args)
		{
			Notebook notebook = new Notebook();
			notebook.AddNote(new Note("Note 1", "Body of note 1"));
			notebook.AddNote(new Note("Note 2", "Body of note 2"));
			notebook.AddNote(new Note("I have no imagination", "Text"));

			IAbstractIterator it = notebook.GetIterator();
			for (Note note = it.First(); !it.IsDone; note = it.Next())
			{
				note.Show();
			}
		}
	}
}
