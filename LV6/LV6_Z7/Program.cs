﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV6_Z7
{
	class Program
	{
		static void Main(string[] args)
		{
			StringChecker checker = new StringLengthChecker(6);
			StringDigitChecker digitChecker = new StringDigitChecker();
			StringUpperCaseChecker upperCaseChecker = new StringUpperCaseChecker();
			StringLowerCaseChecker lowerCaseChecker = new StringLowerCaseChecker();

			PasswordValidator validator = new PasswordValidator(checker);
			validator.AddChecker(digitChecker);
			validator.AddChecker(upperCaseChecker);
			validator.AddChecker(lowerCaseChecker);

			string password1 = "password";
			string password2 = "Passw0rd";
			Console.Write(password1 + ": ");
			Console.WriteLine(checker.Check(password1) ? "valid" : "invalid");
			Console.Write(password2 + ": ");
			Console.WriteLine(checker.Check(password2) ? "valid" : "invalid");
		}
	}
}
