﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV6_Z7
{
	class StringLowerCaseChecker : StringChecker
	{
		protected override bool PerformCheck(string stringToCheck)
		{
			return stringToCheck.Any(char.IsLower);
		}
	}
}
