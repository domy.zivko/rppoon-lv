﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV6_Z7
{
	class StringLengthChecker : StringChecker
	{
		private int minimumLength;

		public StringLengthChecker(int minimumLength)
		{
			this.minimumLength = minimumLength;
		}

		protected override bool PerformCheck(string stringToCheck)
		{
			return stringToCheck.Length >= minimumLength;
		}
	}
}
