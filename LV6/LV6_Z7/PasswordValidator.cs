﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV6_Z7
{
	class PasswordValidator
	{
		private StringChecker baseChecker;
		private StringChecker lastChecker;

		public PasswordValidator(StringChecker stringChecker)
		{
			this.baseChecker = stringChecker;
			this.lastChecker = stringChecker;
		}

		public void AddChecker(StringChecker stringChecker)
		{
			lastChecker.SetNext(stringChecker);
			lastChecker = stringChecker;
		}

		public bool ValidatePassword(string password)
		{
			return baseChecker.Check(password);
		}
	}
}
