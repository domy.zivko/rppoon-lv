﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV7_Z5
{
	class Program
	{
		static void Main(string[] args)
		{
			DVD dvd = new DVD("MATLAB", DVDType.SOFTWARE, 119);
			VHS vhs = new VHS("Titanic", 19.99);
			Book book = new Book("The Hunger Games", 24.99);

			Console.WriteLine(dvd);
			Console.WriteLine(vhs);
			Console.WriteLine(book);
			Console.WriteLine();

			BuyVisitor visitor = new BuyVisitor();
			Console.WriteLine(dvd.Accept(visitor));
			Console.WriteLine(vhs.Accept(visitor));
			Console.WriteLine(book.Accept(visitor));
		}
	}
}
