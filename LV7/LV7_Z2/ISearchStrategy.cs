﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV7_Z2
{
	interface ISearchStrategy
	{
		int Search(double[] array, double element);
	}
}
