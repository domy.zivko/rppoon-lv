﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV7_Z2
{
	class NumberSequence
	{
		private double[] sequence;
		private int sequenceSize;
		private SortStrategy sortStrategy;
		private ISearchStrategy searchStrategy;

		public NumberSequence(int sequenceSize)
		{
			this.sequenceSize = sequenceSize;
			this.sequence = new double[sequenceSize];
		}

		public NumberSequence(double[] array) : this(array.Length)
		{
			array.CopyTo(sequence, 0);
		}

		public int Length { get { return sequenceSize; } }

		public void InsertAt(int index, double value)
		{
			sequence[index] = value;
		}

		public void SetSortStrategy(SortStrategy strategy)
		{
			this.sortStrategy = strategy;
		}

		public void SetSearchStrategy(ISearchStrategy strategy)
		{
			this.searchStrategy = strategy;
		}

		public void Sort()
		{
			sortStrategy.Sort(sequence);
		}

		public int Search(double element)
		{
			return searchStrategy.Search(sequence, element);
		}

		public override string ToString()
		{
			StringBuilder builder = new StringBuilder();
			foreach(double element in sequence)
			{
				builder.Append(element).Append(Environment.NewLine);
			}
			return builder.ToString();
		}
	}
}
