﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV7_Z2
{
	class LinearSearch : ISearchStrategy
	{
		public int Search(double[] array, double element)
		{
			for(int i = 0; i < array.Length; i++)
			{
				if (array[i].Equals(element)) return i;
			}
			return -1;
		}
	}
}
