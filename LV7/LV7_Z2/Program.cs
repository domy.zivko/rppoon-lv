﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV7_Z2
{
	class Program
	{
		static void Main(string[] args)
		{
			Random rand = new Random();
			double[] numbers = new double[10];
			for(int i = 0; i < numbers.Length; i++)
			{
				numbers[i] = rand.Next(10);
			}

			NumberSequence sequence = new NumberSequence(numbers);
			Console.WriteLine(sequence);

			TestSearchStrategy(sequence, new LinearSearch());
		}

		private static void TestSearchStrategy(NumberSequence sequence, ISearchStrategy strategy)
		{
			sequence.SetSearchStrategy(strategy);
			int index = sequence.Search(5);
			Console.WriteLine(index);
		}
	}
}
