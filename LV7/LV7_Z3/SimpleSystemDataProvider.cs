﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV7_Z3
{
	class SimpleSystemDataProvider
	{
		private PerformanceCounter cpuCounter;
		private PerformanceCounter ramCounter;
		private List<Logger> loggers;

		public SimpleSystemDataProvider()
		{
			this.cpuCounter = new PerformanceCounter("Processor", "% processor time", "_Total");
			this.ramCounter = new PerformanceCounter("Memory", "Available MBytes");
			this.loggers = new List<Logger>();
		}

		public float CPULoad {
			get { return cpuCounter.NextValue(); }
		}

		public float AvailableRAM {
			get { return ramCounter.NextValue(); }
		}

		public void Attach(Logger logger)
		{
			if (!loggers.Contains(logger))
				loggers.Add(logger);
		}

		public void Detach(Logger logger)
		{
			loggers.Remove(logger);
		}

		public void Notify()
		{
			foreach(Logger logger in loggers)
			{
				logger.Log(this);
			}
		}
	}
}
