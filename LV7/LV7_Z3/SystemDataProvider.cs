﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV7_Z3
{
	class SystemDataProvider : SimpleSystemDataProvider
	{
		private float previousCPULoad;
		private float previousRAMAvailable;

		public SystemDataProvider() : base()
		{
			this.previousCPULoad = this.CPULoad;
			this.previousRAMAvailable = this.AvailableRAM;
		}

		public float GetCPULoad()
		{
			float currentLoad = CPULoad;
			if (currentLoad != previousCPULoad)
				Notify();

			previousCPULoad = currentLoad;
			return currentLoad;
		}

		public float GetAvailableRAM()
		{
			float currentRAMAvailable = AvailableRAM;
			if (currentRAMAvailable != previousRAMAvailable)
				Notify();

			previousRAMAvailable = currentRAMAvailable;
			return currentRAMAvailable;
		}
	}
}
