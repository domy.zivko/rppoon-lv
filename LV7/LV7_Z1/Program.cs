﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV7_Z1
{
	class Program
	{
		static void Main(string[] args)
		{
			Random rand = new Random();
			double[] numbers = new double[10];
			for(int i = 0; i < numbers.Length; i++)
			{
				numbers[i] = rand.Next(10);
			}

			NumberSequence sequence = new NumberSequence(numbers);
			Console.WriteLine(sequence);

			TestSortStrategy(sequence, new SequentialSort());
			TestSortStrategy(sequence, new BubbleSort());
			TestSortStrategy(sequence, new CombSort());
		}

		private static void TestSortStrategy(NumberSequence sequence, SortStrategy strategy)
		{
			sequence.SetSortStrategy(strategy);
			sequence.Sort();
			Console.WriteLine(sequence);
		}
	}
}
