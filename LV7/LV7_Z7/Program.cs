﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV7_Z7
{
	class Program
	{
		static void Main(string[] args)
		{
			DVD dvd = new DVD("MATLAB", DVDType.SOFTWARE, 119);
			VHS vhs = new VHS("Titanic", 19.99);
			Book book = new Book("The Hunger Games", 24.99);

			Cart cart = new Cart();
			cart.AddItem(dvd);
			cart.AddItem(vhs);
			cart.AddItem(book);
			Console.WriteLine(cart);

			BuyVisitor buyVisitor = new BuyVisitor();
			Console.WriteLine(cart.Accept(buyVisitor));

			RentVisitor rentVisitor = new RentVisitor();
			Console.WriteLine(cart.Accept(rentVisitor));
		}
	}
}
