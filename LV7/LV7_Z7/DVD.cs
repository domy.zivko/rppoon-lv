﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV7_Z7
{
	public enum DVDType
	{
		SOFTWARE, MOVIE
	}

	class DVD : IItem
	{
		public string Description { get; private set; }
		public double Price { get; private set; }
		public DVDType Type { get; private set; }

		public DVD(string description, DVDType type, double price)
		{
			this.Description = description;
			this.Type = type;
			this.Price = price;
		}

		public override string ToString()
		{
			return "DVD: " + Type + " " + Description + Environment.NewLine + " -> Price: " + Price;
		}

		public double Accept(IVisitor visitor)
		{
			return visitor.Visit(this);
		}
	}
}
