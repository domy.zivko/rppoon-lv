﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV7_Z7
{
	class RentVisitor : IVisitor
	{
		private const double DVDTax = 0.18;
		private const double VHSTax = 0.10;
		private const double bookTax = 0.10;

		public double Visit(DVD DVDItem)
		{
			if (DVDItem.Type.Equals(DVDType.SOFTWARE))
				return DVDItem.Price * (1 + DVDTax);
			else
				return DVDItem.Price * (1 + DVDTax) * 0.1;
		}

		public double Visit(VHS VHSItem)
		{
			return VHSItem.Price * (1 + VHSTax) * 0.1;
		}

		public double Visit(Book bookItem)
		{
			return bookItem.Price * (1 + bookTax) * 0.1;
		}
	}
}
