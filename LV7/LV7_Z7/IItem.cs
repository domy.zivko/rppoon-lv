﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV7_Z7
{
	interface IItem
	{
		double Accept(IVisitor visitor);
	}
}
