﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV7_Z7
{
	class Cart
	{
		private readonly List<IItem> items = new List<IItem>();

		public void AddItem(IItem item)
		{
			items.Add(item);
		}

		public void RemoveItem(IItem item)
		{
			items.Remove(item);
		}

		public double Accept(IVisitor visitor)
		{
			double total = 0;
			foreach(IItem item in items)
			{
				total += item.Accept(visitor);
			}
			return total;
		}

		public override string ToString()
		{
			StringBuilder builder = new StringBuilder("Cart contents:\n--------------------\n");
			foreach(IItem item in items)
			{
				builder.Append(item.ToString())
					.Append(Environment.NewLine)
					.Append(Environment.NewLine);
			}
			builder.Append("--------------------");
			return builder.ToString();
		}
	}
}
