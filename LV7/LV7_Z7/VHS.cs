﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV7_Z7
{
	class VHS : IItem
	{
		public string Title { get; private set; }
		public double Price { get; private set; }

		public VHS(string title, double price)
		{
			this.Title = title;
			this.Price = price;
		}

		public override string ToString()
		{
			return "VHS: " + Title + Environment.NewLine + " -> Price: " + Price;
		}

		public double Accept(IVisitor visitor)
		{
			return visitor.Visit(this);
		}
	}
}
