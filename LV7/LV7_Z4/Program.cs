﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LV7_Z4
{
	class Program
	{
		static void Main(string[] args)
		{
			ConsoleLogger consoleLogger = new ConsoleLogger();
			FileLogger fileLogger = new FileLogger("log.txt");

			SystemDataProvider provider = new SystemDataProvider();
			provider.Attach(consoleLogger);
			provider.Attach(fileLogger);

			while (true)
			{
				provider.GetCPULoad();
				provider.GetAvailableRAM();
				Thread.Sleep(1000);
			}
		}
	}
}
