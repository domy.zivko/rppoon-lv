﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV7_Z4
{
	class SystemDataProvider : SimpleSystemDataProvider
	{
		private float previousCPULoad;
		private float previousRAMAvailable;

		public SystemDataProvider() : base()
		{
			this.previousCPULoad = this.CPULoad;
			this.previousRAMAvailable = this.AvailableRAM;
		}

		public float GetCPULoad()
		{
			float currentLoad = CPULoad;
			float difference = Math.Abs(currentLoad - previousCPULoad);
			float differencePercent = difference / previousCPULoad;

			if (differencePercent >= 0.1)
				Notify();

			previousCPULoad = currentLoad;
			return currentLoad;
		}

		public float GetAvailableRAM()
		{
			float currentRAMAvailable = AvailableRAM;
			float difference = Math.Abs(currentRAMAvailable - previousRAMAvailable);
			float differencePercent = difference / previousRAMAvailable;

			if (differencePercent >= 0.1)
				Notify();

			previousRAMAvailable = currentRAMAvailable;
			return currentRAMAvailable;
		}
	}
}
