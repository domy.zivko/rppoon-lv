﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV7_Z4
{
	class FileLogger : Logger
	{
		private string filePath;

		public FileLogger(string filePath)
		{
			this.filePath = filePath;
		}

		public void Log(SimpleSystemDataProvider provider)
		{
			StreamWriter writer = new StreamWriter(filePath, true);
			writer.WriteLine(DateTime.Now + "-> CPU load: " + provider.CPULoad + 
				" Available RAM: " + provider.AvailableRAM);
			writer.Close();
		}
	}
}
