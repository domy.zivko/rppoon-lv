﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV2 {

	/*
	 * Zadaci 1, 2, 3
	 * 
	class Program
	{
		static void Main(string[] args)
		{
			DiceRoller diceRoller = new DiceRoller();
			Random randomGenerator = new Random();

			FillWithDice(diceRoller);
			diceRoller.RollAllDice();
			PrintRollResults(diceRoller.GetRollingResults());
		}

		private static DiceRoller FillWithDice(DiceRoller diceRoller)
		{
			for(int i = 0; i < 10; i++)
			{
				diceRoller.InsertDie(new Die(6)); // Za zadatak 1 i 3
				//diceRoller.InsertDie(new Die(6, randomGenerator)); // Za zadatak 2
			}
			return diceRoller;
		}

		private static void PrintRollResults(IList<int> rollResults)
		{
			foreach(int r in rollResults)
			{
				Console.Write(r + " ");
			}
			Console.WriteLine();
		}
	}
	*/

	class Program
	{
		static void Main(string[] args)
		{
			DiceRoller diceRoller = new DiceRoller();
			ConsoleLogger logger = new ConsoleLogger();

			FillWithDice(diceRoller);
			diceRoller.RollAllDice();
			logger.Log(diceRoller);
		}

		private static DiceRoller FillWithDice(DiceRoller diceRoller)
		{
			for (int i = 0; i < 10; i++)
			{
				diceRoller.InsertDie(new Die(6));
			}
			return diceRoller;
		}
	}
}
