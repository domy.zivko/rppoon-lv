﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV2 {

	/*
	 * Primjer 4 (Zadatak 4)
	 * 
	interface ILogger 
	{
		void Log(string message);
	}
	*/

	interface ILogger
	{
		void Log(ILogable data);
	}
}
