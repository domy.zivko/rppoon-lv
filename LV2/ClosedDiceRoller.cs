﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV2
{
	class ClosedDiceRoller : IDiceRoller
	{
		private List<Die> dice;
		private List<int> resultForEachRoll;

		public ClosedDiceRoller(int diceCount, int numberOfSides)
		{
			this.dice = new List<Die>(diceCount);
			for(int i = 0; i < diceCount; i++)
			{
				this.dice.Add(new Die(numberOfSides));
			}
			this.resultForEachRoll = new List<int>();
		}

		public void RollAllDice()
		{
			resultForEachRoll.Clear();
			foreach(Die die in dice)
			{
				resultForEachRoll.Add(die.Roll());
			}
		}
	}
}
