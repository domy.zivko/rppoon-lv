﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV2
{
	class FlexibleDiceRoller : IDiceRoller, IDiceHolder
	{
		private List<Die> dice;
		private List<int> resultForEachRoll;

		public FlexibleDiceRoller()
		{
			this.dice = new List<Die>();
			this.resultForEachRoll = new List<int>();
		}

		public void InsertDie(Die die)
		{
			dice.Add(die);
		}

		public void RemoveAllDice()
		{
			dice.Clear();
			resultForEachRoll.Clear();
		}

		public void RollAllDice()
		{
			resultForEachRoll.Clear();
			foreach(Die die in dice)
			{
				resultForEachRoll.Add(die.Roll());
			}
		}

		// Zadatak 7
		public void RemoveDiceWithSides(int numberOfSides)
		{
			dice.RemoveAll(die => die.NumberOfSides == numberOfSides);
		}
	}
}
