﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV2
{
	/*
	 * Zadatak 4
	 * 
	class ConsoleLogger : ILogger
	{
		public void Log(string message)
		{
			Console.WriteLine(message);
		}
	}
	*/

	// Zadatak 5
	class ConsoleLogger : ILogger
	{
		public void Log(ILogable logable)
		{
			Console.WriteLine(logable.GetStringRepresentation());
		}
	}
}
