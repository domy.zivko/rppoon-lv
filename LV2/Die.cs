﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV2 {

	/*
	 * Zadatak 2
	 * 
	class Die 
	{
		private int numberOfSides;
		private Random randomGenerator;

		public Die(int numberOfSides, Random randomGenerator)
		{
			this.numberOfSides = numberOfSides;
			this.randomGenerator = randomGenerator;
		}

		public int Roll()
		{
			int rolledNumber = randomGenerator.Next(1, numberOfSides + 1);
			return rolledNumber;
		}

	}
	*/

	/*
	 * Zadatak 3
	 * 
	class Die 
	{
		private int numberOfSides;
		private RandomGenerator randomGenerator;

		public Die(int numberOfSides)
		{
			this.numberOfSides = numberOfSides;
			this.randomGenerator = RandomGenerator.GetInstance();
		}

		public int Roll()
		{
			int rolledNumber = randomGenerator.NextInt(1, numberOfSides + 1);
			return rolledNumber;
		}
	}
	*/

	// Zadatak 7
	class Die
	{
		private RandomGenerator randomGenerator;

		public int NumberOfSides { get; }

		public Die(int numberOfSides)
		{
			this.NumberOfSides = numberOfSides;
			this.randomGenerator = RandomGenerator.GetInstance();
		}

		public int Roll()
		{
			int rolledNumber = randomGenerator.NextInt(1, NumberOfSides + 1);
			return rolledNumber;
		}

	}
}
