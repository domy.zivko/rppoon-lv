﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV2 {

	/*
	 * Zadaci 1, 2, 3
	 * 
	class DiceRoller {

		private List<Die> dice;
		private List<int> resultForEachRoll;

		public DiceRoller()
		{
			this.dice = new List<Die>();
			this.resultForEachRoll = new List<int>();
		}

		public void InsertDie(Die die)
		{
			dice.Add(die);
		}

		public void RollAllDice()
		{
			resultForEachRoll.Clear();
			foreach(Die die in dice)
			{
				resultForEachRoll.Add(die.Roll());
			}
		}
	}
	*/

	/*
	 * Zadatak 4
	 * 
	class DiceRoller
	{
		private List<Die> dice;
		private List<int> resultForEachRoll;
		private ILogger logger;

		public DiceRoller()
		{
			this.dice = new List<Die>();
			this.resultForEachRoll = new List<int>();
			this.logger = new ConsoleLogger();
		}

		public void SetLogger(ILogger logger)
		{
			this.logger = logger;
		}

		public void InsertDie(Die die)
		{
			dice.Add(die);
		}

		public void RollAllDice()
		{
			resultForEachRoll.Clear();
			foreach (Die die in dice)
			{
				resultForEachRoll.Add(die.Roll());
			}
		}

		public IList<int> GetRollingResults()
		{
			return new System.Collections.ObjectModel.ReadOnlyCollection<int>(resultForEachRoll);
		}

		public int DieCount {
			get { return dice.Count; }
		}

		public void LogRollingResults()
		{
			foreach (int result in resultForEachRoll)
			{
				logger.Log(result.ToString());
			}
		}
	}
	*/

	// Zadatak 5
	class DiceRoller : ILogable
	{
		private List<Die> dice;
		private List<int> resultForEachRoll;

		public DiceRoller()
		{
			this.dice = new List<Die>();
			this.resultForEachRoll = new List<int>();
		}

		public void InsertDie(Die die)
		{
			dice.Add(die);
		}

		public void RollAllDice()
		{
			resultForEachRoll.Clear();
			foreach (Die die in dice)
			{
				resultForEachRoll.Add(die.Roll());
			}
		}

		public IList<int> GetRollingResults()
		{
			return new System.Collections.ObjectModel.ReadOnlyCollection<int>(resultForEachRoll);
		}

		public int DieCount {
			get { return dice.Count; }
		}

		public string GetStringRepresentation()
		{
			StringBuilder stringBuilder = new StringBuilder();
			foreach (int result in resultForEachRoll)
			{
				stringBuilder.Append(result.ToString());
			}
			return stringBuilder.ToString();
		}
	}
}
