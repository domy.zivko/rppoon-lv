﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV4_Z3
{
	class Program
	{
		static void Main(string[] args)
		{
			List<IRentable> rentables = new List<IRentable>();
			rentables.Add(new Book("The Hunger Games"));
			rentables.Add(new Video("The Shawshank Redemption"));

			RentingConsolePrinter printer = new RentingConsolePrinter();
			printer.DisplayItems(rentables);
			printer.PrintTotalPrice(rentables);
		}
	}
}
