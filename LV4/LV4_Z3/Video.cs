﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV4_Z3
{
	class Video : IRentable
	{
		private readonly double BaseVideoPrice = 9.99;
		public string Name { get; private set; }
		public string Description { get { return Name; } }

		public Video(string name)
		{
			this.Name = name;
		}

		public double CalculatePrice()
		{
			return BaseVideoPrice;
		}
	}
}
