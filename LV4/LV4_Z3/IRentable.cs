﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV4_Z3
{
	interface IRentable
	{
		string Description { get; }
		double CalculatePrice();
	}
}
