﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV4_Z3
{
	class Book : IRentable
	{
		private readonly double BaseBookPrice = 3.99;
		public string Title { get; private set; }
		public string Description { get { return Title; } }

		public Book(string title)
		{
			this.Title = title;
		}

		public double CalculatePrice()
		{
			return BaseBookPrice;
		}
	}
}
