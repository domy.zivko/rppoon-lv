﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV4_Z1
{
	class Analyzer3rdParty
	{
		public double[] PerRowAverage(double[][] data)
		{
			int rowCount = data.Length;
			double[] results = new double[rowCount];
			for (int i = 0; i < rowCount; i++)
			{
				results[i] = data[i].Average();
			}
			return results;
		}

		// Metoda podrzava retke razlicitih duljina. Tek kasnije sam u dodatku vidio da se 
		// duljina redaka mozda moze pretpostaviti da je fiksna
		public double[] PerColumnAverage(double[][] data)
		{
			double[] sums = CalculateColumnSums(data);
			double[] lengths = CountColumnElements(data);

			int columnCount = sums.Length;
			double[] perColumnAverage = new double[columnCount];
			for (int i = 0; i < columnCount; i++)
			{
				perColumnAverage[i] = sums[i] / lengths[i];
			}
			return perColumnAverage;
		}

		private double[] CalculateColumnSums(double[][] data)
		{
			int rowCount = data.Length;
			int columnCount = CountColumns(data);
			double[] columnSums = new double[columnCount];
			for (int i = 0; i < rowCount; i++)
			{
				for (int j = 0; j < data[i].Length; j++)
				{
					columnSums[j] += data[i][j];
				}
			}
			return columnSums;
		}

		private double[] CountColumnElements(double[][] data)
		{
			int rowCount = data.Length;
			int columnCount = CountColumns(data);
			double[] columnLengths = new double[columnCount];
			for (int i = 0; i < rowCount; i++)
			{
				for (int j = 0; j < data[i].Length; j++)
				{
					columnLengths[j]++;
				}
			}
			return columnLengths;
		}

		private int CountColumns(double[][] data)
		{
			int rowCount = data.Length;
			int columnCount = 0;
			for (int i = 0; i < rowCount; i++)
			{
				if (data[i].Length > columnCount)
					columnCount = data[i].Length;
			}
			return columnCount;
		}
	}
}
