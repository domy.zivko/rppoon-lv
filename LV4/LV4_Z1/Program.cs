﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV4_Z1
{
	class Program
	{
		static void Main(string[] args)
		{
			Analyzer3rdParty analyzerService = new Analyzer3rdParty();
			Adapter analyzer = new Adapter(analyzerService);

			Dataset variedDataset = new Dataset("variableRowsExample.csv");

			PrintAnalytics(analyzer.CalculateAveragePerRow(variedDataset));
			PrintAnalytics(analyzer.CalculateAveragePerColumn(variedDataset));
		}

		private static void PrintAnalytics(double[] analytics)
		{
			foreach (double number in analytics)
			{
				Console.Write(number + " ");
			}
			Console.WriteLine();
		}
	}
}
