﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV4_Z1
{
	class Adapter : IAnalytics
	{
		private Analyzer3rdParty analyticsService;

		public Adapter(Analyzer3rdParty service)
		{
			this.analyticsService = service;
		}

		private double[][] ConvertData(Dataset dataset)
		{
			IList<List<double>> data = dataset.GetData();

			int rowCount = data.Count;
			double[][] convertedData = new double[rowCount][];
			for (int i = 0; i < rowCount; i++)
			{
				convertedData[i] = data[i].ToArray();
			}
			return convertedData;
		}

		public double[] CalculateAveragePerColumn(Dataset dataset)
		{
			double[][] data = ConvertData(dataset);
			return analyticsService.PerColumnAverage(data);
		}

		public double[] CalculateAveragePerRow(Dataset dataset)
		{
			double[][] data = ConvertData(dataset);
			return analyticsService.PerRowAverage(data);
		}
	}
}
