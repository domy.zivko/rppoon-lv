﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV4_Z2
{
	class Program
	{
		static void Main(string[] args)
		{
			TestAnalyzer3rdParty();
			Console.WriteLine("----------");
			TestAdapter();
		}

		private static void TestAnalyzer3rdParty()
		{
			Analyzer3rdParty analyzerService = new Analyzer3rdParty();
			double[][] data = new double[][]{
				new double[] { 4, 3, 2, 1 },
				new double[] { 50, 40, 30 },
				new double[] { 25, 20, 15, 10, 5 },
			};

			PrintAnalytics(analyzerService.PerRowAverage(data));
			PrintAnalytics(analyzerService.PerColumnAverage(data));
		}

		private static void TestAdapter()
		{
			Analyzer3rdParty analyzerService = new Analyzer3rdParty();
			Adapter analyzer = new Adapter(analyzerService);

			Dataset basicDataset = new Dataset("example.csv");
			Dataset variedDataset = new Dataset("variableRowsExample.csv");

			PrintAnalytics(analyzer.CalculateAveragePerRow(basicDataset));
			PrintAnalytics(analyzer.CalculateAveragePerColumn(basicDataset));
			Console.WriteLine();
			PrintAnalytics(analyzer.CalculateAveragePerRow(variedDataset));
			PrintAnalytics(analyzer.CalculateAveragePerColumn(variedDataset));
		}

		private static void PrintAnalytics(double[] analytics)
		{
			foreach (double number in analytics)
			{
				Console.Write(number + " ");
			}
			Console.WriteLine();
		}
	}
}
