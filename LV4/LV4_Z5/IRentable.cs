﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV4_Z5
{
	interface IRentable
	{
		string Description { get; }
		double CalculatePrice();
	}
}
