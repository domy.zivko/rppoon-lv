﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV4_Z5
{
	class DiscountedItem : RentableDecorator 
	{
		private readonly double discountPercentage;

		public DiscountedItem(IRentable rentable, double discountPercentage) : base(rentable)
		{
			this.discountPercentage = discountPercentage;
		}

		public override double CalculatePrice()
		{
			return base.CalculatePrice() * (1 - discountPercentage/100);
		}

		public override string Description {
			get {
				return $"{base.Description} now at {discountPercentage}% off";
			}
		}
	}
}
