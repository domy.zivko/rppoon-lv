﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV4_Z5
{
	class Program
	{
		static void Main(string[] args)
		{
			List<IRentable> rentables = new List<IRentable>();
			rentables.Add(new Book("The Hunger Games"));
			rentables.Add(new Video("The Shawshank Redemption"));
			rentables.Add(new HotItem(new Book("Lord of the Rings")));
			rentables.Add(new HotItem(new Video("Inception")));

			RentingConsolePrinter printer = new RentingConsolePrinter();
			printer.DisplayItems(rentables);
			printer.PrintTotalPrice(rentables);
			Console.WriteLine("----------");

			List<IRentable> flashSale = new List<IRentable>();
			rentables.ForEach(r => flashSale.Add(new DiscountedItem(r, 20)));

			printer.DisplayItems(flashSale);
			printer.PrintTotalPrice(flashSale);
		}
	}
}
