﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV4_Z7
{
	class Program
	{
		static void Main(string[] args)
		{
			IRegistrationValidator validator = new RegistrationValidator();

			bool entryValid;
			do
			{
				UserEntry entry = UserEntry.ReadUserFromConsole();
				entryValid = validator.IsUserEntryValid(entry);
				if (!entryValid)
					Console.WriteLine("Invalid entry! Please try again.");
			} while (!entryValid);

			Console.WriteLine("Entry is valid!");
		}
	}
}
