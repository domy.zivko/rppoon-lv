﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV4_Z7
{
	class EmailValidator : IEmailValidatorService
	{
		public bool IsValidAddress(string candidate)
		{
			bool domainValid = candidate.EndsWith(".com") || candidate.EndsWith(".hr");
			return domainValid && candidate.Contains('@');
		}
	}
}
