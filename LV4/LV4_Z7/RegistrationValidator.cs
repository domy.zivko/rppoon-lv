﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV4_Z7
{
	class RegistrationValidator : IRegistrationValidator
	{
		private IEmailValidatorService emailValidator;
		private IPasswordValidatorService passwordValidator;

		public RegistrationValidator()
		{
			this.emailValidator = new EmailValidator();
			this.passwordValidator = new PasswordValidator(6);
		}

		public bool IsUserEntryValid(UserEntry entry)
		{
			bool emailValid = emailValidator.IsValidAddress(entry.Email);
			bool passwordValid = passwordValidator.IsValidPassword(entry.Password);
			return emailValid && passwordValid;
		}
	}
}
