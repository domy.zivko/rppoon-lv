﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV4_Z7
{
	class UserEntry
	{
		public string Email { get; set; }
		public string Password { get; set; }

		private UserEntry() { }

		public static UserEntry ReadUserFromConsole()
		{
			UserEntry entry = new UserEntry();
			Console.WriteLine("Enter email: ");
			entry.Email = Console.ReadLine();
			Console.WriteLine("Enter password: ");
			entry.Password = Console.ReadLine();
			return entry;
		}
	}
}
