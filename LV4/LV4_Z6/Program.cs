﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV4_Z6
{
	class Program
	{
		private IEmailValidatorService emailValidator = new EmailValidator();
		private IPasswordValidatorService passwordValidator = new PasswordValidator(6);

		private Program()
		{
			TestCredentials("electronic@mail.com", "Passw0rd!");
			Console.WriteLine();
			TestCredentials("mechanic.mail", "lowercase!");
		}

		private void TestCredentials(string email, string password)
		{
			bool emailValid = emailValidator.IsValidAddress(email);
			Console.WriteLine(emailValid ? "Email valid" : "Email invalid");

			bool passwordValid = passwordValidator.IsValidPassword(password);
			Console.WriteLine(passwordValid ? "Password valid" : "Password invalid");
		}

		static void Main(string[] args)
		{
			new Program();
		}
	}
}
