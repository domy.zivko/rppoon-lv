﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV4_Z4
{
	class Program
	{
		// Odgovor: Hit stavke u ispisu uz sebe imaju oznaku "Trending: ", te im je cijena viša za 1.99
		static void Main(string[] args)
		{
			List<IRentable> rentables = new List<IRentable>();
			rentables.Add(new Book("The Hunger Games"));
			rentables.Add(new Video("The Shawshank Redemption"));
			rentables.Add(new HotItem(new Book("Lord of the Rings")));
			rentables.Add(new HotItem(new Video("Inception")));

			RentingConsolePrinter printer = new RentingConsolePrinter();
			printer.DisplayItems(rentables);
			printer.PrintTotalPrice(rentables);
		}
	}
}
