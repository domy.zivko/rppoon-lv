﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV3_Z6
{
	class Program
	{
		static void Main(string[] args)
		{
			NotificationManager manager = new NotificationManager();
			NotificationBuilder builder = new NotificationBuilder();
			NotificationDirector director = new NotificationDirector(builder);

			ConsoleNotification infoNotification = director.ConstructInfoNotification("Dominik Zivko");
			ConsoleNotification alertNotification = director.ConstructAlertNotification("Dominik Zivko");
			ConsoleNotification errorNotification = director.ConstructErrorNotification("Dominik Zivko");

			manager.Display(infoNotification);
			manager.Display(alertNotification);
			manager.Display(errorNotification);
		}
	}
}
