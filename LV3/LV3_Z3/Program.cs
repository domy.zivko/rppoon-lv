﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV3_Z3
{
	class Program
	{
		// Odgovor: S obzirom da imamo samo jednu instancu, uporaba Loggera na bilo kojem mjestu u 
		// programu ce zapisivati u datoteku koja je zadnji put postavljena, neovisno o tome 
		// gdje je postavljena.

		static void Main(string[] args)
		{
			Logger logger = Logger.GetInstance();
			Console.WriteLine("Logging message: Message to log");
			logger.Log("Message to log");
		}
	}
}
