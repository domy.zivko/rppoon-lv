﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV3_Z1
{
	// Odgovor: Duboko kopiranje je potrebno pri kloniranju klase Dataset jer ona sadrzi listu 
	// podataka cije je elemente takoder potrebno kopirati. Buduci da su i elementi te liste 
	// takoder liste, i njene elemente treba kopirati.
	class Program
	{
		static void Main(string[] args)
		{
			Dataset dataset = new Dataset("file.csv");
			Dataset clonedDataset = (Dataset)dataset.Clone();

			PrintDataset(dataset);
			Console.WriteLine();
			PrintDataset(clonedDataset);

			clonedDataset.ClearData();

			Console.WriteLine();
			PrintDataset(dataset);
			Console.WriteLine();
			PrintDataset(clonedDataset);
		}

		private static void PrintDataset(Dataset dataset)
		{
			foreach (List<string> row in dataset.GetData())
			{
				foreach (string item in row)
				{
					Console.Write(item + " ");
				}
				Console.WriteLine();
			}
		}
	}
}
