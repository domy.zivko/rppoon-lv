﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV3_Z4
{
	class Program
	{
		static void Main(string[] args)
		{
			NotificationManager manager = new NotificationManager();
			ConsoleNotification notification = new ConsoleNotification(
				"Dominik Zivko", "Title", "Text", DateTime.Now, Category.INFO, ConsoleColor.DarkGreen);

			manager.Display(notification);
		}
	}
}
