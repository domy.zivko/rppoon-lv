﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV3_Z5
{
	class Dataset : Prototype
	{
		private List<List<string>> data;

		public Dataset()
		{
			this.data = new List<List<string>>();
		}

		public Dataset(string filePath) : this()
		{
			LoadDataFromCSV(filePath);
		}

		public Dataset(Dataset dataset)
		{
			this.data = new List<List<string>>();
			foreach (List<string> row in dataset.data)
			{
				List<string> rowClone = new List<string>(row);
				this.data.Add(rowClone);
			}
		}

		public void LoadDataFromCSV(string filePath)
		{
			using(System.IO.StreamReader reader = new System.IO.StreamReader(filePath))
			{
				string line;
				while((line = reader.ReadLine()) != null)
				{
					List<string> row = new List<string>();
					string[] items = line.Split(',');
					foreach(string item in items)
					{
						row.Add(item);
					}
					data.Add(row);
				}
			}
		}

		public IList<List<string>> GetData()
		{
			return new System.Collections.ObjectModel.ReadOnlyCollection<List<string>>(data);
		}

		public void ClearData()
		{
			data.Clear();
		}

		public Prototype Clone() => new Dataset(this);
	}

}
