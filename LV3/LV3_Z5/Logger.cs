﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV3_Z5
{
	class Logger
	{
		private static Logger instance;

		public string FilePath { get; set; }

		private Logger()
		{
			FilePath = "log.txt";
		}

		public static Logger GetInstance()
		{
			if (instance == null)
				instance = new Logger();

			return instance;
		}

		public void Log(string message)
		{
			using(System.IO.StreamWriter writer = new System.IO.StreamWriter(FilePath, true))
			{
				writer.WriteLine(message);
			}
		}
	}
}
