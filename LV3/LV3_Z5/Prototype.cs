﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV3_Z5
{
	interface Prototype
	{
		Prototype Clone();
	}
}
