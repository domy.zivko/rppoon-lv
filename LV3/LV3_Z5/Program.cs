﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV3_Z5
{
	class Program
	{
		static void Main(string[] args)
		{
			NotificationManager manager = new NotificationManager();
			NotificationBuilder builder = new NotificationBuilder();

			ConsoleNotification defaultNotification = builder.Build();

			builder.SetAuthor("Dominik Zivko");
			builder.SetTitle("Spam");
			builder.SetText("Important spam");
			builder.SetTime(DateTime.Now.AddHours(-1));
			builder.SetLevel(Category.ALERT);
			builder.SetColor(ConsoleColor.DarkRed);
			ConsoleNotification customNotification = builder.Build();

			manager.Display(defaultNotification);
			manager.Display(customNotification);
		}
	}
}
