﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV3_Z7
{
	public enum Category { ERROR, ALERT, INFO }

	class ConsoleNotification : Prototype
	{
		public String Author { get; private set; }
		public String Title { get; private set; }
		public String Text { get; private set; }
		public DateTime Timestamp { get; private set; }
		public Category Level { get; private set; }
		public ConsoleColor Color { get; private set; }

		public ConsoleNotification(String author, String title, String text, 
			DateTime time, Category level, ConsoleColor color)
		{
			this.Author = author;
			this.Title = title;
			this.Text = text;
			this.Timestamp = time;
			this.Level = level;
			this.Color = color;
		}

		// Odgovor: U ovom slucaju razlika izmedu plitkog i dubokog kopiranja je efektivno 
		// nepostojeca jer su svi atributi klase nepromjenjivi objekti. Zbog toga nema potrebe 
		// za njihovim kopiranjem vec je dovoljno da kopija klase pokazuje na iste objekte.
		public Prototype Clone()
		{
			return (Prototype)MemberwiseClone();
		}
	}
}
