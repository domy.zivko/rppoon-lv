﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV3_Z7
{
	class NotificationDirector
	{
		private IBuilder builder;

		public NotificationDirector(IBuilder builder)
		{
			this.builder = builder;
		}

		public ConsoleNotification ConstructInfoNotification(String author)
		{
			builder.SetAuthor(author);
			builder.SetTitle("Notice");
			builder.SetText("Information");
			builder.SetTime(DateTime.Now);
			builder.SetLevel(Category.INFO);
			builder.SetColor(ConsoleColor.White);
			return builder.Build();
		}

		public ConsoleNotification ConstructAlertNotification(String author)
		{
			builder.SetAuthor(author);
			builder.SetTitle("Warning");
			builder.SetText("Important information");
			builder.SetTime(DateTime.Now);
			builder.SetLevel(Category.ALERT);
			builder.SetColor(ConsoleColor.DarkYellow);
			return builder.Build();
		}

		public ConsoleNotification ConstructErrorNotification(String author)
		{
			builder.SetAuthor(author);
			builder.SetTitle("Error");
			builder.SetText("Error message");
			builder.SetTime(DateTime.Now);
			builder.SetLevel(Category.ERROR);
			builder.SetColor(ConsoleColor.Red);
			return builder.Build();
		}
	}
}
