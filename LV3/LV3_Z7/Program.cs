﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV3_Z7
{
	class Program
	{
		static void Main(string[] args)
		{
			NotificationManager manager = new NotificationManager();
			NotificationBuilder builder = new NotificationBuilder();
			NotificationDirector director = new NotificationDirector(builder);

			ConsoleNotification notification = director.ConstructInfoNotification("Dominik Zivko");
			ConsoleNotification cloneNotification = (ConsoleNotification)notification.Clone();

			manager.Display(notification);
			manager.Display(cloneNotification);
		}
	}
}
