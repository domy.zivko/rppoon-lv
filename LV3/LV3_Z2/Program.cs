﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV3_Z2
{
	class Program
	{
		static void Main(string[] args)
		{
			MatrixGenerator matrixGenerator = MatrixGenerator.GetInstance();
			double[][] matrix = matrixGenerator.GenerateRandomMatrix(5, 8);
			PrintMatrix(matrix);
		}

		private static void PrintMatrix(double[][] matrix)
		{
			foreach(double[] row in matrix)
			{
				foreach(double number in row)
				{
					Console.Write(number.ToString("F") + "\t");
				}
				Console.WriteLine();
			}
		}
	}
}
