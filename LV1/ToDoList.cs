﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV1 {

	class ToDoList {

		private readonly List<Note> tasks = new List<Note>();

		public Note this[int i] {
			get { return tasks[i]; }
			private set { tasks[i] = value; }
		}

		public int TaskCount {
			get { return tasks.Count; }
		}

		public void AddTask(Note task)
		{
			tasks.Add(task);
		}

		public void RemoveTask(Note task)
		{
			tasks.Remove(task);
		}

		public override string ToString()
		{
			string printout = "";

			foreach (var task in tasks)
			{
				printout += $"{task}\n";
			}

			return printout;
		}

	}
}
