﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV1 {

	class Program {

		static void Main(string[] args)
		{
			TestNotes();
			TestToDoList();
		}

		static void TestNotes()
		{
			Note note = new Note("Student 1");
			Console.WriteLine(note);
			note = new Note("Student 2", "Tekst biljeske");
			Console.WriteLine(note);
			note = new TimestampNote("Student 3", "Tekst biljeske", 1);
			Console.WriteLine(note);
		}

		static void TestToDoList()
		{
			ToDoList toDoList = new ToDoList();

			InputTasks(toDoList);
			PrintTasks(toDoList);
			RemoveTopPriorityTasks(toDoList);
			PrintTasks(toDoList);
		}

		private static void InputTasks(ToDoList toDoList)
		{
			int n;
			do { Console.WriteLine("Enter the number of tasks"); }
			while (!int.TryParse(Console.ReadLine(), out n) || n < 3);

			for (int i = 0; i < n; i++)
			{
				Console.WriteLine($"Task {i}:");
				InputSingleTask(toDoList);
			}
		}

		private static void InputSingleTask(ToDoList toDoList)
		{
			Console.Write("Author: ");
			string author = Console.ReadLine();
			Console.Write("Text: ");
			string text = Console.ReadLine();
			Console.Write("Priority: ");
			int.TryParse(Console.ReadLine(), out int priority);

			Note task = new Note(author, text, priority);
			toDoList.AddTask(task);
		}

		private static void PrintTasks(ToDoList toDoList)
		{
			Console.WriteLine(toDoList);
		}

		private static void RemoveTopPriorityTasks(ToDoList toDoList)
		{
			List<Note> topTasks = FindTopPriorityTasks(toDoList);
			foreach (var task in topTasks.ToList())
			{
				toDoList.RemoveTask(task);
			}
		}

		private static List<Note> FindTopPriorityTasks(ToDoList toDoList)
		{
			List<Note> topTasks = new List<Note>();
			int topPriority = FindTopPriority(toDoList);

			// inače implementirati foreach za prolazak kroz taskove, ali to mi se čini overkill za ovaj zadatak
			for (int i = 0; i < toDoList.TaskCount; i++)
			{
				if (toDoList[i].Priority == topPriority)
					topTasks.Add(toDoList[i]);
			}

			return topTasks;
		}

		private static int FindTopPriority(ToDoList toDoList)
		{
			int topPriority = toDoList[0].Priority;
			for (int i = 1; i < toDoList.TaskCount; i++)
			{
				int priority = toDoList[i].Priority;
				if (priority < topPriority)
					topPriority = priority;
			}
			return topPriority;
		}

	}
}
