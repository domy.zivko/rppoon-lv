﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV1 {

	class TimestampNote : Note {

		public DateTime DateTime { get; set; }

		public TimestampNote(string author, string text, int priority)
			: this(DateTime.Now, author, text, priority) { }

		public TimestampNote(DateTime dateTime, string author, string text, int priority)
			: base(author, text, priority)
		{
			DateTime = dateTime;
		}

		public override string ToString()
		{
			return $"{DateTime} ({Priority}) {Author}: {Text}";
		}

	}
}
