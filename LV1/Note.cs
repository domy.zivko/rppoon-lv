﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV1 {

	class Note {

		private string author;
		private string text;
		private int priority;

		public Note(string author) : this(author, "") { }

		public Note(string author, string text) : this(author, text, 0) { }

		public Note(string author, string text, int priority)
		{
			this.author = author;
			this.text = text;
			this.priority = priority;
		}

		public string GetAuthor() { return author; }
		public string GetText() { return text; }
		public int GetPriority() { return priority; }

		public void SetText(string text) { this.text = text; }
		public void SetPriority(int priority) { this.priority = priority; }

		public string Author {
			get => GetAuthor();
			private set { author = value; }
		}

		public string Text {
			get => GetText();
			set => SetText(value);
		}

		public int Priority {
			get => GetPriority();
			set => SetPriority(value);
		}

		public override string ToString()
		{
			return $"({priority}) {Author}: {text}";
		}

	}
}
